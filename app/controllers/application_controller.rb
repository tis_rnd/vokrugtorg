class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_categories_link

  private

  def set_categories_link
    @first_category = Spree::Taxon.first
  end
end

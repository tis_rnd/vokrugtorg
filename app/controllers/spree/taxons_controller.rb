module Spree
  class TaxonsController < Spree::StoreController
    rescue_from ActiveRecord::RecordNotFound, with: :render_404
    helper 'spree/products'

    respond_to :html

    def show
      @taxon = Taxon.friendly.find(params[:id])
      return unless @taxon

      @searcher = build_searcher(params.merge(taxon: @taxon.id, include_images: true))
      @products = @searcher.retrieve_products
      @taxonomies = Spree::Taxonomy.includes(root: :children)
      @last_category = @taxon.children.empty?
      
      # crutch for special categories
      if @taxon.name == 'Категории'
        @special_categories = SpecialCategory.all
      end
    end

    private

    def accurate_title
      @taxon.try(:seo_title) || super
    end
  end
end

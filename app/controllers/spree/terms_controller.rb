module Spree
  class TermsController < Spree::StoreController
    before_action :set_background
    layout 'spree/layouts/whitespace'

    def index
    end

    private

    def set_background
      @custom_background = 'bg-terms'
    end
  end
end

module Spree
  class AboutController < Spree::StoreController
    before_filter :set_bg

    def index
    end

    private

    def set_bg
      @whitespace = true
    end
  end
end

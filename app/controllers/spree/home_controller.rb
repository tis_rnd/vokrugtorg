module Spree
  class HomeController < Spree::StoreController
    helper 'spree/products'
    respond_to :html

    def index
      @searcher = build_searcher(params.merge(include_images: true))
      @products = @searcher.retrieve_products.includes(:possible_promotions)
      @taxonomies = Spree::Taxonomy.includes(root: :children)
      @featured_products = Spree::Product.where(:featured_check => true)
      @new_products = Spree::Product.where(:new_check => true)
      # get special categories for custom sidebar
      @special_categories = SpecialCategory.where('parent_id > 0')
      # activate sidebar
      @sidebar = true
      # shim for FeaturedCarousel
      # TODO: make this as white human would do
      @carousel_links = [
        Spree::Product.find_by_id(1327658),
        Spree::Product.find_by_id(1248268),
        Spree::Product.find_by_id(1172789),
        Spree::Product.find_by_id(1270320),
        Spree::Product.find_by_id(1401822)
      ]
    end

    def place_order
      begin
        order = Spree::Order.find_by_number(order_params[:order])
        MainMailer.new_application(order_params, order).deliver_now
      rescue
        flash[:error] = "Произошла непредвиденная ошибка, повторите попытку позже"
        redirect_to :root and return
      end
      order.empty!
      redirect_to :success_order
    end

    def show_order
      @order_number = params[:order]
    end

    def success_order
    end

    private

    def order_params
      params[:offer].permit(:name, :email, :phone, :order)
    end

  end
end

module Spree
  class GuaranteesController < Spree::StoreController
    layout 'spree/layouts/whitespace'

    def index
    end

    def feedback
      MainMailer.new_feedback(feedback_params).deliver_now
      render :nothing => true
    end

    private

    def feedback_params
      params.permit(:invoice, :contacts, :item_name, :message)
    end

  end
end

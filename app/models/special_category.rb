class SpecialCategory < ActiveRecord::Base
  has_one :taxon, class_name: 'Spree::Taxon', foreign_key: :id, primary_key: :children_id
end

class MainMailer < ApplicationMailer

  def new_application(data, order)
    redirect_to :root if data[:phone].empty? && data[:email].empty?

    @data = data
    @products = order.present? ? order.products : []
    @line_items = order.line_items

    mail(to: 'info@vokrugtorg.ru', subject: 'Заявка на покупку с Vokrugtorg')
  end

  def new_feedback(data)
    redirect_to :root if data[:invoice].empty? or data[:contacts].empty?

    @data = data
    mail(to: 'info@vokrugtorg.ru', subject: 'Заявка из "Гарантии" с Vokrugtorg')
  end
end

class ApplicationMailer < ActionMailer::Base
  default from: "cc@vokrugtorg.ru"
  layout 'mailer'
end

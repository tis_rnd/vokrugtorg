ready = ->

  $featured = $('#feature .item, #all-products .item')
  $new = $('#new .item')
  $cart = $()
  $featuredTab = $('#featured-tab')
  $newTab = $('#new-tab')
  $featureSpace = $('#feature')
  $newSpace = $('#new')
  $similars = $('.similar-item')

  cart_itemcount = ->
    $.ajax
      url: Spree.pathFor("cart_link"),
      success: (data) ->
        $('#itemcount').html data
  
  ############ CAROUSEL #############

  featureCarousel = ->
    if $('#new').is('.slick-initialized')
      $('#new').slick('unslick')
      
    $('#feature').not('.slick-initialized').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

  newCarousel = ->
    if $('#feature').is('.slick-initialized')
      $('#feature').slick('unslick')

    $('#new').not('.slick-initialized').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

  increaseValue = ($item) ->
    $item.html(parseInt($item.html()) + 1)

  decreaseValue = ($item) ->
    if parseInt($item.html()) > 1
      $item.html(parseInt($item.html()) - 1)

  updateCart = ($data) ->
    # shim for custom mount point
    mp = $('#api-shim').attr('mount-point')
    if mp.length
      mp = '/' + mp
    else
      mp = '/'

    $.post(mp + 'orders/populate', $data, ->
      cart_itemcount()
      alert('Товар был успешно добавлен в корзину!'))
    return

  initializeCart = ($cart) ->
    $cart.click -> 
      id = $cart.attr('variant-id')
      quantity = $('#'+id).find('#val-control').html()

      order = {
        'variant_id': id,
        'quantity': quantity
      }

      updateCart(order)
      return

  # initialize increase and decrease item count + cart
  if $featured.length
    $featured.each (k, item) ->
      $item = $(item)
      $item.find('#lt-control').click -> decreaseValue($item.find('#val-control'))
      $item.find('#gt-control').click -> increaseValue($item.find('#val-control'))
      initializeCart($item.find("#cart-control"))

  if $new.length
    $new.each (k, item) -> 
      $item = $(item)
      $item.find('#lt-control').click -> decreaseValue($item.find('#val-control'))
      $item.find('#gt-control').click -> increaseValue($item.find('#val-control'))
      initializeCart($item.find("#cart-control"))

  $featuredTab.click ->
    $newSpace.hide()
    $featureSpace.show()
    featureCarousel()

  $newTab.click ->
    $featureSpace.hide()
    $newSpace.show()
    newCarousel()

  featureCarousel()

  ############### SIDEBAR ###############
  
  $sidebar = $('.sidebar-container')
  $items = $('.sidebar-container > .item')

  $items.each (k, target) ->
    $(target).mouseenter (item) ->
      # close all menus first
      $items.find('.children-container').css('display', 'none')
      $z = $(item.target).find('.children-container')
      # set vertical alignment for bottom menus (to not let it overflow with footer)
      if k > 11
        $z.css('top', -$z.height() + $z.parent().height() + 'px')
      $z.css('display', 'block')

  $items.each (k, target) ->
    $(target).click (item) ->
      # close all menus first
      $items.find('.children-container').css('display', 'none')
      $z = $(item.target)
      # in case that user clicks exactly on span with text
      if $z.attr('class') == 'inner-text'
        $z = $z.parent().find('.children-container')
      else
        $z = $z.find('.children-container')
      # set vertical alignment for bottom menus (to not let it overflow with footer)
      if k > 11
        $z.css('top', -$z.height() + $z.parent().height() + 'px')

      $z.css('display', 'block')

  $items.mouseleave (item) ->
    if $(item.target).attr('class') == 'children-container'
      $(item.target).css('display', 'none')
    else
      $(item.target).find('.children-container').css('display', 'none')

  # close all items on leave
  $sidebar.mouseleave ->
    $items.find('.children-container').css('display', 'none')

  ############### SIMILAR PRODUCTS ###############

  # initialize increase and decrease item count + cart
  if $similars.length
    $similars.each (k, item) ->
      $item = $(item)
      $item.find('#lt-control').click -> decreaseValue($item.find('#val-control'))
      $item.find('#gt-control').click -> increaseValue($item.find('#val-control'))
      initializeCart($item.find("#cart-control"))

  return

$(document).ready(ready)
$(document).on('page:load', ready)

ready = ->
  $feedback = $('#feedback')
  $thanks = $('#thanks')
  $invoice = $feedback.find('input[name="invoice_number"]')
  $contacts = $feedback.find('input[name="contacts"]')
  $item_name = $feedback.find('input[name="item_name"]')
  $message = $feedback.find('textarea[name="message"]')
  data = {}

  check_fields = ->
    $invoice.css('border', '')
    $contacts.css('border', '')
    if $invoice.val().length == 0
      $invoice.css('border-color', 'red')
    if $contacts.val().length == 0
      $contacts.css('border-color', 'red')

  $feedback.find('input[type="submit"]').click ->
    # check if we need to highlight fields with an error
    check_fields()
    # check necessary fields
    if $invoice.val().length == 0
      alert('Вам необходимо указать номер накладной или номер заказа.')
    else if $contacts.val().length == 0
      alert('Вам необходимо указать контактные данные по которым с Вами свяжется наш менеджер.')
    else
      data['invoice'] = $invoice.val()
      data['contacts'] = $contacts.val()
      data['item_name'] = $item_name.val()
      data['message'] = $message.val()
      $.post('guarantees/feedback', data, ->
        $feedback.css('display', 'none')
        $thanks.css('display', 'block')
      )

$(document).ready(ready)
$(document).on('page:load', ready)

Spree.ready ($) ->
  if ($ 'form#update-cart').is('*')
    # for deleting items from cart
    deleteButtons = ($ 'form#update-cart a.delete').show()
    for item in deleteButtons
      $(item).click ->
        $(item).parents('.line-item').first().find('input.line_item_quantity').val 0
        $(item).parents('form').first().submit()
        false

  # for updating items in cart
  ($ 'form#update-cart').submit ->
    ($ 'form#update-cart #update-button').attr('disabled', true)

  return

cart_itemcount = ->
  $.ajax
    url: Spree.pathFor("cart_link"),
    success: (data) ->
      $('#itemcount').html data

ready = ->
  cart_itemcount()

$(document).ready(ready)
$(document).on('page:load', ready)

Rails.application.routes.draw do

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  # mount Spree::Core::Engine, at: '/'
  mount_point = Rails.configuration.mount_point

  mount Spree::Core::Engine, at: "/#{mount_point}"
    # root to: "catalogue#index"
    get "/#{mount_point}", to: "home#index"

  # setting up developing mode
  root to: 'home#in_development'

  # temporary rewrite order cycle
  Spree::Core::Engine.add_routes do
    # order
    get "/show_order", to: "home#show_order", as: 'show_order'
    get "/success_order", to: "home#success_order", as: 'success_order'
    put "/place_order", to: "home#place_order", as: 'place_order'
    # custom pages
    get "/about", to: "about#index", as: 'about'
    get "/b2b", to: "b2b#index", as: 'b2b'
    get "/terms", to: "terms#index", as: 'terms'
    get "/events", to: "events#index", as: 'events'
    get "/guarantees", to: "guarantees#index", as: 'guarantees'
    post "/guarantees/feedback", to: "guarantees#feedback", as: 'feedback'
  end

end

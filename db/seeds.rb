# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# uncomment for first run
# Spree::Core::Engine.load_seed if defined?(Spree::Core)
# Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

puts "===== (seeding Special Categories) ====="

if SpecialCategory.find_by_children_id(1).nil?
  ##### Main category 'Категории' #####
  SpecialCategory.create!(parent_id: '0', children_id: '1', name: 'Категории')

  SpecialCategory.create!(parent_id: '1', children_id: '2', name: 'Компьютеры, Ноутбуки, Планшеты')
  SpecialCategory.create!(parent_id: '2', children_id: '111360') # Планшеты
  SpecialCategory.create!(parent_id: '2', children_id: '10429')  # Ноутбуки
  SpecialCategory.create!(parent_id: '2', children_id: '5187')   # Компьютеры, неттопы
  SpecialCategory.create!(parent_id: '2', children_id: '255197') # Моноблоки
  SpecialCategory.create!(parent_id: '2', children_id: '206766') # Аксессуары к ноутбукам
  SpecialCategory.create!(parent_id: '2', children_id: '330149') # Аксессуары к планшетам

  SpecialCategory.create!(parent_id: '1', children_id: '3', name: 'Портативная электроника')
  SpecialCategory.create!(parent_id: '3', children_id: '111361') # Смартфоны, сотовые телефоны, умные часы и аксессуары
  SpecialCategory.create!(parent_id: '3', children_id: '143849') # Автомобильные GPS-навигаторы и видеорегистраторы
  SpecialCategory.create!(parent_id: '3', children_id: '330150') # Аксессуары к смартфонам,телефонам
  SpecialCategory.create!(parent_id: '3', children_id: '143685') # Калькуляторы
  SpecialCategory.create!(parent_id: '3', children_id: '100932') # MP3/MPEG4-плееры, диктофоны, FM-трансмиттеры, радиоприемники, ЗУ и аксессуары
  SpecialCategory.create!(parent_id: '3', children_id: '281950') # Радиостанции и радио-няни
  SpecialCategory.create!(parent_id: '3', children_id: '385721') # Умные часы и браслеты
  SpecialCategory.create!(parent_id: '3', children_id: '154904') # Электронные книги

  SpecialCategory.create!(parent_id: '1', children_id: '4', name: 'Носители информации')
  SpecialCategory.create!(parent_id: '4', children_id: '15676')  # Внешние HDD
  SpecialCategory.create!(parent_id: '4', children_id: '93067')  # USB-флеш драйвы
  SpecialCategory.create!(parent_id: '4', children_id: '103167') # Карты памяти, Card Reader
  SpecialCategory.create!(parent_id: '4', children_id: '126412') # CD / DVD / Blu-Ray -диски, дискеты , miniDV

  SpecialCategory.create!(parent_id: '1', children_id: '5', name: 'Расходные материалы')
  SpecialCategory.create!(parent_id: '5', children_id: '198240') # Чипы, барабаны, тонеры, запасные части для сборки и восстановления картриджей
  SpecialCategory.create!(parent_id: '5', children_id: '198241') # Оригинальные совместимые картриджи для аппаратов Brother, Xerox, Canon, HP, Epson, Samsung
  SpecialCategory.create!(parent_id: '5', children_id: '143732') # Расходные материалы HP
  SpecialCategory.create!(parent_id: '5', children_id: '143734') # Расходные материалы Brother, Toshiba, Kyocera Mita, Ricoh, Sharp, Panasonic, Lexmark
  SpecialCategory.create!(parent_id: '5', children_id: '143733') # Расходные материалы Canon, Epson, Xerox, Samsung
  SpecialCategory.create!(parent_id: '5', children_id: '143735') # Клейкая лента упаковочная
  SpecialCategory.create!(parent_id: '5', children_id: '176598') # Расходные материалы TallyGenicom, Dymo, LetraTag, Konica-Minolta, OKI
  SpecialCategory.create!(parent_id: '5', children_id: '392743') # Бумага офисная
  SpecialCategory.create!(parent_id: '5', children_id: '393083') # Широкоформатная бумага
  SpecialCategory.create!(parent_id: '5', children_id: '393271') # Термобумага для факсов
  SpecialCategory.create!(parent_id: '5', children_id: '393277') # Бумага для цветной печати
  SpecialCategory.create!(parent_id: '5', children_id: '393289') # Чистящие средства
  SpecialCategory.create!(parent_id: '5', children_id: '393291') # Фотобумага

  SpecialCategory.create!(parent_id: '1', children_id: '6', name: 'Периферия, Офисное оборудование, UPS')
  SpecialCategory.create!(parent_id: '6', children_id: '5195')   # МФУ, Принтеры, Копиры
  SpecialCategory.create!(parent_id: '6', children_id: '5194')   # Сканеры
  SpecialCategory.create!(parent_id: '6', children_id: '386893') # 3D Принтеры 
  SpecialCategory.create!(parent_id: '6', children_id: '5189')   # UPS
  SpecialCategory.create!(parent_id: '6', children_id: '387843') # Сетевые фильтры и стабилизаторы
  SpecialCategory.create!(parent_id: '6', children_id: '5188')   # Мониторы
  SpecialCategory.create!(parent_id: '6', children_id: '16754')  # Проекторы
  SpecialCategory.create!(parent_id: '6', children_id: '5201')   # Телефония
  SpecialCategory.create!(parent_id: '6', children_id: '16755')  # Уничтожители, ламинаторы, брошюраторы, обложки, пружинки
  SpecialCategory.create!(parent_id: '6', children_id: '251495') # Торговое оборудование
  SpecialCategory.create!(parent_id: '6', children_id: '331514') # Экраны,пульты Д\У к экранам,системы голосования,крепления к проекторам
  SpecialCategory.create!(parent_id: '6', children_id: '331515') # Интерактивные доски,доп.устройства

  SpecialCategory.create!(parent_id: '1', children_id: '7', name: 'Сетевое оборудование, модемы, IP-камеры')
  SpecialCategory.create!(parent_id: '7', children_id: '5191')   # Коммутаторы, Беспроводное оборудование, Медиа конвертеры, Принт-серверы
  SpecialCategory.create!(parent_id: '7', children_id: '5192')   # Модемы
  SpecialCategory.create!(parent_id: '7', children_id: '5190')   # Сетевые карты
  SpecialCategory.create!(parent_id: '7', children_id: '254776') # Монтажное оборудование Hyperline
  SpecialCategory.create!(parent_id: '7', children_id: '52871')  # Кабель сетевой, телефонный
  SpecialCategory.create!(parent_id: '7', children_id: '5208')   # Монтажное оборудование
  SpecialCategory.create!(parent_id: '7', children_id: '141689') # IP Телефония
  SpecialCategory.create!(parent_id: '7', children_id: '422752') # Кроссы, патч-корды

  SpecialCategory.create!(parent_id: '1', children_id: '8', name: 'Комплектующие')
  SpecialCategory.create!(parent_id: '8', children_id: '5198')   # Материнские платы
  SpecialCategory.create!(parent_id: '8', children_id: '5197')   # Процессоры
  SpecialCategory.create!(parent_id: '8', children_id: '67860')  # Охлаждающие системы
  SpecialCategory.create!(parent_id: '8', children_id: '10928')  # Память
  SpecialCategory.create!(parent_id: '8', children_id: '60142')  # Винчестеры
  SpecialCategory.create!(parent_id: '8', children_id: '255681') # SSD-накопители
  SpecialCategory.create!(parent_id: '8', children_id: '175024') # Внешние контейнеры для HDD, Mobile racks
  SpecialCategory.create!(parent_id: '8', children_id: '5199')   # Видеокарты
  SpecialCategory.create!(parent_id: '8', children_id: '5200')   # Дисководы (FDD)
  SpecialCategory.create!(parent_id: '8', children_id: '5204')   # Оптические приводы
  SpecialCategory.create!(parent_id: '8', children_id: '11080')  # Контроллеры, порты
  SpecialCategory.create!(parent_id: '8', children_id: '12422')  # Корпуса
  SpecialCategory.create!(parent_id: '8', children_id: '22093')  # Блоки питания
  SpecialCategory.create!(parent_id: '8', children_id: '100935') # Звуковые платы, TV-тюнеры, платы видеомонтажа

  SpecialCategory.create!(parent_id: '1', children_id: '9', name: 'Фото, Видео, Телевизоры, Электроника')
  SpecialCategory.create!(parent_id: '9', children_id: '63332')  # Телевизоры, аксессуары к телевизорам
  SpecialCategory.create!(parent_id: '9', children_id: '5146')   # Цифровые фотокамеры, фоторамки
  SpecialCategory.create!(parent_id: '9', children_id: '69347')  # Цифровые видеокамеры
  SpecialCategory.create!(parent_id: '9', children_id: '60150')  # Аксессуары, сумки, объективы, штативы, вспышки для фото-и видеодеокамер
  SpecialCategory.create!(parent_id: '9', children_id: '230634') # Спутниковое телевидение
  SpecialCategory.create!(parent_id: '9', children_id: '143843') # Игровые консоли
  SpecialCategory.create!(parent_id: '9', children_id: '100934') # DVD проигрыватели, Дом. кинотеатры
  SpecialCategory.create!(parent_id: '9', children_id: '156132') # Метеостанции
  SpecialCategory.create!(parent_id: '9', children_id: '164182') # HDD мультимедийные плееры 
  SpecialCategory.create!(parent_id: '9', children_id: '280892') # Музыкальные центры
  SpecialCategory.create!(parent_id: '9', children_id: '324652') # Цифровые ТВ приставки

  SpecialCategory.create!(parent_id: '1', children_id: '10', name: 'Системы безопасности и видеонаблюдения')
  SpecialCategory.create!(parent_id: '10', children_id: '230432') # Камеры видеонаблюдения
  SpecialCategory.create!(parent_id: '10', children_id: '410470') # Кабель для видеонаблюдения
  SpecialCategory.create!(parent_id: '10', children_id: '410474') # Видеорегистраторы
  SpecialCategory.create!(parent_id: '10', children_id: '410877') # Домофоны
  SpecialCategory.create!(parent_id: '10', children_id: '410878') # Web-камеры, Колонки, Наушники, Микрофоны

  SpecialCategory.create!(parent_id: '1', children_id: '11', name: 'Web-камеры, Колонки, Наушники, Микрофоны')
  SpecialCategory.create!(parent_id: '11', children_id: '100933') # Web-камеры
  SpecialCategory.create!(parent_id: '11', children_id: '5202')   # Колонки
  SpecialCategory.create!(parent_id: '11', children_id: '55675')  # Наушники и микрофоны

  SpecialCategory.create!(parent_id: '1', children_id: '12', name: 'Клавиатуры, Манипуляторы')
  SpecialCategory.create!(parent_id: '12', children_id: '5205')   # Клавиатуры, мыши
  SpecialCategory.create!(parent_id: '12', children_id: '143684') # Графические планшеты
  SpecialCategory.create!(parent_id: '12', children_id: '55676')  # Манипуляторы

  SpecialCategory.create!(parent_id: '1', children_id: '13', name: 'Аксессуары')
  SpecialCategory.create!(parent_id: '13', children_id: '5206')   # Коврики
  SpecialCategory.create!(parent_id: '13', children_id: '137992') # USB-гаджеты
  SpecialCategory.create!(parent_id: '13', children_id: '67680')  # Компьютерные аксессуары: стойки, папки для CD
  SpecialCategory.create!(parent_id: '13', children_id: '5207')   # Шлейфы, кабели, переходники
  SpecialCategory.create!(parent_id: '13', children_id: '7163')   # Селекторы принтеров, переключатели, разветвители сигнала

  SpecialCategory.create!(parent_id: '1', children_id: '14', name: 'Электроинструмент')
  SpecialCategory.create!(parent_id: '14', children_id: '281582') # Дрели, Шуруповерты, Гайковерты, Дрели-миксеры
  SpecialCategory.create!(parent_id: '14', children_id: '281583') # Перфораторы
  SpecialCategory.create!(parent_id: '14', children_id: '281965') # Отбойные молотки
  SpecialCategory.create!(parent_id: '14', children_id: '281585') # Шлифовальные машины
  SpecialCategory.create!(parent_id: '14', children_id: '281605') # Краскопульты, аэрографы
  SpecialCategory.create!(parent_id: '14', children_id: '302812') # Многофункциональные инструменты
  SpecialCategory.create!(parent_id: '14', children_id: '281589') # Пилы, Лобзики
  SpecialCategory.create!(parent_id: '14', children_id: '281588') # Штроборезы
  SpecialCategory.create!(parent_id: '14', children_id: '281607') # Фены, Термопистолеты
  SpecialCategory.create!(parent_id: '14', children_id: '281970') # Ножницы
  SpecialCategory.create!(parent_id: '14', children_id: '281977') # Пылесосы строительные и принадлежности
  SpecialCategory.create!(parent_id: '14', children_id: '281989') # Фрезеры
  SpecialCategory.create!(parent_id: '14', children_id: '283601') # Станки точильные, шлифовальные
  SpecialCategory.create!(parent_id: '14', children_id: '281986') # Гвозди и скобозабиватели, Степлеры
  SpecialCategory.create!(parent_id: '14', children_id: '281982') # Рубанки, Рейсмусы
  SpecialCategory.create!(parent_id: '14', children_id: '303956') # Плиткорезы
  SpecialCategory.create!(parent_id: '14', children_id: '315318') # Компрессоры, Пневматическое оборудование
  SpecialCategory.create!(parent_id: '14', children_id: '315319') # Сварочное оборудование, Инверторы
  SpecialCategory.create!(parent_id: '14', children_id: '315861') # Вибротехника

  SpecialCategory.create!(parent_id: '1', children_id: '15', name: 'Ручной инструмент')
  SpecialCategory.create!(parent_id: '15', children_id: '281604') # Измерительный инструмент
  SpecialCategory.create!(parent_id: '15', children_id: '285967') # Наборы ручного инструмента
  SpecialCategory.create!(parent_id: '15', children_id: '285966') # Штукатурно-малярный инструмент
  SpecialCategory.create!(parent_id: '15', children_id: '281606') # Пистолеты
  SpecialCategory.create!(parent_id: '15', children_id: '285998') # Лестницы, Стремянки, Верстаки
  SpecialCategory.create!(parent_id: '15', children_id: '285964') # Зажимной инструмент
  SpecialCategory.create!(parent_id: '15', children_id: '285965') # Ключи
  SpecialCategory.create!(parent_id: '15', children_id: '285968') # Отвёртки, Молотки
  SpecialCategory.create!(parent_id: '15', children_id: '285969') # Паяльное оборудование
  SpecialCategory.create!(parent_id: '15', children_id: '285970') # Пилы, ножовки по дереву и металлу, лобзики,Стусла
  SpecialCategory.create!(parent_id: '15', children_id: '285976') # Средства индивидуальной защиты
  SpecialCategory.create!(parent_id: '15', children_id: '281584') # Сумки и ящики для инструментов
  SpecialCategory.create!(parent_id: '15', children_id: '298533') # Надфили, напильники, рашпили, стамески
  SpecialCategory.create!(parent_id: '15', children_id: '299117') # Разное
  SpecialCategory.create!(parent_id: '15', children_id: '302355') # Хозтовары
  SpecialCategory.create!(parent_id: '15', children_id: '303956') # Плиткорезы
  SpecialCategory.create!(parent_id: '15', children_id: '321976') # Губцевый инструмент

  SpecialCategory.create!(parent_id: '1', children_id: '16', name: 'Бензоинструмент, Садовый Инструмент, Садовая техника')
  SpecialCategory.create!(parent_id: '16', children_id: '281956') # Газонокосилки, Аэраторы, Триммеры, Электро и бензокосы
  SpecialCategory.create!(parent_id: '16', children_id: '309949') # Садовые измельчители
  SpecialCategory.create!(parent_id: '16', children_id: '281931') # Бензопилы, Бензорезы
  SpecialCategory.create!(parent_id: '16', children_id: '281964') # Кусторезы, Высоторезы, Садовые пилы
  SpecialCategory.create!(parent_id: '16', children_id: '286011') # Мотобуры
  SpecialCategory.create!(parent_id: '16', children_id: '286014') # Воздуходувки, Садовые пылесосы
  SpecialCategory.create!(parent_id: '16', children_id: '286001') # Мотоблоки, Культиваторы
  SpecialCategory.create!(parent_id: '16', children_id: '283584') # Мойки
  SpecialCategory.create!(parent_id: '16', children_id: '281969') # Насосы
  SpecialCategory.create!(parent_id: '16', children_id: '286002') # Мотопомпы
  SpecialCategory.create!(parent_id: '16', children_id: '286004') # Тачки
  SpecialCategory.create!(parent_id: '16', children_id: '296356') # Садовые принадлежности
  SpecialCategory.create!(parent_id: '16', children_id: '286013') # Снегоуборочная техника
  SpecialCategory.create!(parent_id: '16', children_id: '318744') # Тепловые пушки
  SpecialCategory.create!(parent_id: '16', children_id: '285972') # Системы полива, Распрыскиватели, Опрыскиватели, шланги, катушки
  SpecialCategory.create!(parent_id: '16', children_id: '296339') # Ручные инструменты для обработки почвы Буры, Вилы, Грабли, Лопаты, Культиваторы, Корнеудалители
  SpecialCategory.create!(parent_id: '16', children_id: '296346') # Инструменты ручные садовые режущие Кусторезы,  Высоторезы,  Ножницы для стрижки травы, Ножовки садовые, Секаторы, Сучкорезы, Наборы косцов

  SpecialCategory.create!(parent_id: '1', children_id: '17', name: 'Оснастка и расходные материалы для электро-бензоинструмента')
  SpecialCategory.create!(parent_id: '17', children_id: '319078') # Диски отрезные,пильные,шлифовальные
  SpecialCategory.create!(parent_id: '17', children_id: '319079') # Полотна для лобзика, принадлежности для мультитула
  SpecialCategory.create!(parent_id: '17', children_id: '319081') # Сверла,коронки,наборы(Металл,бетон,стекло,плитка,дерево)
  SpecialCategory.create!(parent_id: '17', children_id: '319082') # Отвертки-насадки(биты),наборы,державки
  SpecialCategory.create!(parent_id: '17', children_id: '319083') # Ножи для рубанков
  SpecialCategory.create!(parent_id: '17', children_id: '319084') # Патроны,переходники,смазка для буров
  SpecialCategory.create!(parent_id: '17', children_id: '319085') # Щетки,тарелки для дрелей и УШМ
  SpecialCategory.create!(parent_id: '17', children_id: '319086') # Шлифовальная бумага,лента,круги
  SpecialCategory.create!(parent_id: '17', children_id: '319088') # Буры SDS+,наборы буров,долота
  SpecialCategory.create!(parent_id: '17', children_id: '319089') # Буры SDS-Max,долота
  SpecialCategory.create!(parent_id: '17', children_id: '319365') # Принадлежности к промышленным пылесосам
  SpecialCategory.create!(parent_id: '17', children_id: '320080') # Аккумуляторы и ЗУ к электроинструментам
  SpecialCategory.create!(parent_id: '17', children_id: '355999') # Фрезы
  SpecialCategory.create!(parent_id: '17', children_id: '384096') # Цепи, шины, свечи, заточн. инструмент для пил

  SpecialCategory.create!(parent_id: '1', children_id: '18', name: 'Электротехнические изделия')
  SpecialCategory.create!(parent_id: '18', children_id: '288012') # Светотехника
  SpecialCategory.create!(parent_id: '18', children_id: '306043') # Дверные звонки
  SpecialCategory.create!(parent_id: '18', children_id: '298334') # Батарейки, зарядные устройства, аккумуляторы
  SpecialCategory.create!(parent_id: '18', children_id: '288011') # Кабельно-проводниковая продукция
  SpecialCategory.create!(parent_id: '18', children_id: '288025') # Кабеленесущие системы, электромонтажные изделия
  SpecialCategory.create!(parent_id: '18', children_id: '288030') # Электроустановочные изделия
  SpecialCategory.create!(parent_id: '18', children_id: '288034') # Низковольтное оборудование
  SpecialCategory.create!(parent_id: '18', children_id: '296943') # Инструменты,средства защиты
  SpecialCategory.create!(parent_id: '18', children_id: '302123') # Удлинители
  SpecialCategory.create!(parent_id: '18', children_id: '281962') # Генераторы, Электростанции
  SpecialCategory.create!(parent_id: '18', children_id: '363320') # Стабилизаторы напряжения

  SpecialCategory.create!(parent_id: '1', children_id: '19', name: 'Офисная мебель, сейфы, шкафы')
  SpecialCategory.create!(parent_id: '19', children_id: '62254')  # Мебель для компьютеров
  SpecialCategory.create!(parent_id: '19', children_id: '289363') # Сейфы
  SpecialCategory.create!(parent_id: '19', children_id: '289791') # Шкафы ПРАКТИК
  SpecialCategory.create!(parent_id: '19', children_id: '295816') # Кэшбоксы, ключницы

  puts "===== Done ====="
else
  puts "===== (Special Categories already seeded) ====="
end

# File.open('file.txt', 'a') { |file|
#   items = SpecialCategory.all
#   items.each do |x|
#     if !x.name.nil? or !x.taxon.name.nil?
#       file.write(x.children_id.to_s + ' ' + (x.name? ? x.name : x.taxon.name) + "\n")
#     end
#   end
# }
class AddOrderToSpreeTaxons < ActiveRecord::Migration
  def change
    add_column :spree_taxons, :order, :integer
  end
end

class AddNameToSpecialCategory < ActiveRecord::Migration
  def change
    add_column :special_categories, :name, :string
  end
end

class AddActiveToSpecialCategory < ActiveRecord::Migration
  def change
    add_column :special_categories, :active, :boolean
  end
end

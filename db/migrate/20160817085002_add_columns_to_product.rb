class AddColumnsToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :new_text, :string
    add_column :spree_products, :featured_text, :string
    add_column :spree_products, :new_check, :bool, :default => false
    add_column :spree_products, :featured_check, :bool, :default => false
  end
end

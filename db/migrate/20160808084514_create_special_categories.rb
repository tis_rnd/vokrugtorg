class CreateSpecialCategories < ActiveRecord::Migration
  def change
    create_table :special_categories do |t|
      t.integer :parent_id
      t.integer :children_id

      t.timestamps null: false
    end
  end
end

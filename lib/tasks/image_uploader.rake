namespace :image do
  desc "Fill products with images"
  
  task :create => :environment do
    Utils::ImageUploader.create
  end

end

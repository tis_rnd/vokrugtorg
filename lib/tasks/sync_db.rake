namespace :sync_db do
  desc "Sync Website Database"
  
  task :create => :environment do
    Utils::DB.create
  end

  task :update => :environment do
    Utils::DB.update
  end
end

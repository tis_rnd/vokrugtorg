require 'zip'

module Utils
  class DB
    PROPS_URL = 'http://www.netlab.ru/products/GoodsProperties.zip'
    ITEMS_URL = 'http://www.netlab.ru/products/priceXML.zip'
    PROPS_ZIP_PATH = "#{Rails.root}/public/GoodsProperties.zip"
    ITEMS_ZIP_PATH = "#{Rails.root}/public/priceXML.zip"
    PROPS_PATH = "#{Rails.root}/public/GoodsProperties.xml"
    ITEMS_PATH = "#{Rails.root}/public/Price.xml"

    def self.create
      p '------------------INITIALIZING------------------'
      p '~'

      p '------------------GETING USD CURRENCY------------------'
      usd_currency = self.get_usd_currency
      p '------------------DONE------------------'
      p '~'

      p '------------------GETTING LATEST XML DATABASE------------------'
      self.get_latest_db_files
      p '------------------DONE------------------'
      p '~'

      p '------------------OPENING XML DATABASE------------------'
      doc_props = Nokogiri::XML(File.open(PROPS_PATH)) do |config|
        config.options = Nokogiri::XML::ParseOptions::STRICT | Nokogiri::XML::ParseOptions::NONET | Nokogiri::XML::ParseOptions::NOBLANKS
      end

      doc_items = Nokogiri::XML(File.open(ITEMS_PATH)) do |config|
        config.options = Nokogiri::XML::ParseOptions::STRICT | Nokogiri::XML::ParseOptions::NONET | Nokogiri::XML::ParseOptions::NOBLANKS
      end
      p '------------------DONE------------------'
      p '~'

      p '------------------PARSING PROPERTIES------------------'
      properties = doc_props.xpath("//properties")
      properties.children.each do |x|
        if x.attributes['id']
          # getting and storing item options
          id = x.attributes['id'].text.delete('p')
          name = x.text.gsub(/(<b>)?(<\/b>)?/, '')
          # insert values if not exists
          if !name.blank? && Spree::Property.find_by(:id => id.to_i).nil?
            Spree::Property.create!(:id => id.to_i, :name => name, :presentation => name)
          end
        end
      end

      self.load_default_props

      p '------------------DONE------------------'
      p '~'

      p '------------------PARSING CATEGORIES------------------'
      # parse categories
      categories = doc_items.xpath("//shop/categories")
      categories.children.each do |x|
        if x.attributes['id']
          # getting and storing item options
          id = x.attributes['id'].text.to_i
          order = x.attributes['OrderBy'].text.to_i if x.attributes['OrderBy']
          parent_id = x.attributes['parentId'].text.to_i if x.attributes['parentId']
          name = x.text
          # insert values if not exists
          if Spree::Taxon.find_by_id(id).nil?
            Spree::Taxon.create(:id => id, :taxonomy_id => 1, :order => order || 0,
                                :parent_id => parent_id || 1, :name => name)
          end
        end
      end
      p '------------------DONE------------------'
      p '~'

      p '------------------PARSING ITEMS------------------'
      self.parse_items doc_items, usd_currency
      p '------------------DONE------------------'
      p '~'

      p '------------------SET PROPERTIES TO ITEMS------------------'
      item_properties = doc_props.xpath("//items")
      item_properties.children.each do |x|
        if x.attributes['id']
          # getting item Id
          item = Spree::Product.find_by_id(x.attributes['id'].text.to_i)
          if item.present?
            x.children.each do |y|
              # name ex.: uid, url, priceR, ..
              prop = Spree::Property.find_by_id(y.name.delete('p'))
              Spree::ProductProperty.find_or_create_by(:product_id => item.id, :property_id => prop.id, :value => y.text) if prop.present?
            end
          end
        end
      end
      p '------------------DONE------------------'
      p '~'

      p '------------------SUCCESSFUL------------------'
    end

    #################################

    def self.update
      p '------------------INITIALIZING------------------'
      p '~'

      p '------------------GETING USD CURRENCY------------------'
      usd_currency = self.get_usd_currency
      p '------------------DONE------------------'
      p '~'

      p '------------------GETTING LATEST XML DATABASE------------------'
      self.get_latest_db_files false, true
      p '------------------DONE------------------'
      p '~'

      p '------------------OPENING XML DATABASE------------------'
      doc_items = Nokogiri::XML(File.open(ITEMS_PATH)) do |config|
        config.options = Nokogiri::XML::ParseOptions::STRICT | Nokogiri::XML::ParseOptions::NONET | Nokogiri::XML::ParseOptions::NOBLANKS
      end
      p '------------------DONE------------------'
      p '~'

      p '------------------PARSING ITEMS------------------'
      self.parse_items doc_items, usd_currency, false
      p '------------------DONE------------------'
      p '~'

      p '------------------SUCCESSFUL------------------'
    end

    protected

    def self.get(url)
      Net::HTTP.get URI.parse(url)
    end

    def self.price_with_taxes(item, usd_currency)
      original_price = item['priceD'].to_f
      category = item['categoryId'].to_i
      # # 5 categories are tax-free
      # case category
      # when 111360
      # when 10429
      # when 5187
      # when 255197
      # when 111361
        original_price * usd_currency * 1.1864
      # else
      #   original_price * usd_currency * 1.05
      # end
    end

    def self.load_default_props
      Spree::Taxon.find_or_create_by(:id => 1, :name => "Категории", :taxonomy_id => 1)
    end

    def self.get_usd_currency
      Utils::Currency.get_cbr
    end

    def self.get_latest_db_files props_init=true, items_init=true
      # remove old files if they exist
      File.delete(PROPS_PATH) if File.exist?(PROPS_PATH) if props_init
      File.delete(ITEMS_PATH) if File.exist?(ITEMS_PATH) if items_init

      if props_init
        # download props in zip
        props_zip = self.get(PROPS_URL)
        File.open(PROPS_ZIP_PATH, 'wb') do |file|
          file.write(props_zip)
        end
        # unzip props
        zip_file = Zip::File.open(PROPS_ZIP_PATH)
        zip_file.each do |file|
          file.extract(PROPS_PATH)
        end
      end

      if items_init
        # download items in zip
        items_zip = self.get(ITEMS_URL)
        File.open(ITEMS_ZIP_PATH, 'wb') do |file|
          file.write(items_zip)
        end
        # unzip items
        zip_file = Zip::File.open(ITEMS_ZIP_PATH)
        zip_file.each do |file|
          file.extract(ITEMS_PATH)
        end
      end

      # remove zip
      File.delete(PROPS_ZIP_PATH) if props_init
      File.delete(ITEMS_ZIP_PATH) if items_init
    end

    def self.parse_items doc_items, usd_currency, init=true
      offers = doc_items.xpath("//shop/offers")
      offers.children.each do |x|
        if x.attributes['id']
          tmp = {}
          x.children.each do |y|
            # name ex.: uid, url, priceR, ..
            tmp[y.name] = y.text
          end

          # check and create Product if doesn't exists
          item = Spree::Product.find_by_id(tmp['uid'].to_i)
          if item.nil? and init
            # create Product
            item = Spree::Product.create(:id => tmp['uid'].to_i, :name => tmp['name'],
                                         :shipping_category => Spree::ShippingCategory.first,
                                         :price => self.price_with_taxes(tmp, usd_currency),
                                         :available_on => Time.now)
            # add Category to Product
            taxon = Spree::Taxon.find_by_id(tmp['categoryId'].to_i)
            item.taxons << taxon if !taxon.nil? and item.taxons.exclude?(taxon)
          elsif !item.nil?
            # update a price for an existing Product
            item.price = self.price_with_taxes(tmp, usd_currency)
            if init
              taxon = Spree::Taxon.find_by_id(tmp['categoryId'].to_i)
              item.taxons << taxon if !taxon.nil? and item.taxons.exclude?(taxon)
            end
            item.save
          end

          if init
            # find all properties and store them
            tmp.each do |name, val|
              # look for property and create one if not present
              begin
                prop = Spree::Property.find_or_create_by(:name => name, :presentation => 'nil')
                Spree::ProductProperty.find_or_create_by(:product_id => item.id, :property_id => prop.id, :value => val)
              rescue => e
                p '# error has happened at line #99'
                p e
                p '# - #'
                exit
              end
            end
          end
        end
      end
    end
  end
end

module Utils
  class Currency
    CBR_URL = "http://www.cbr.ru/scripts/"

    def self.get(path)
      self.parse Net::HTTP.get URI.parse(CBR_URL + path)
    end

    def self.post(path, fields)
      self.parse Net::HTTP.post_form URI.parse(CBR_URL + path), fields
    end

    def self.parse(response)
      begin
        target = Nokogiri::XML(response)
        target.xpath('//Valute[@ID="R01235"]').first # USD
      rescue
        puts 'CBR currency is not available'
        exit
      end
    end

    def self.get_cbr
      xml = self.get('XML_daily.asp')
      xml.search('Value').text.gsub(',', '.').to_f
    end
    
  end
end

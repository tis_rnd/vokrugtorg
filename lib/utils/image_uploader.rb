# all files must be located in /public/item-images directory
module Utils
  class ImageUploader
    def self.create
      p '------------------INITIALIZING------------------'
      p '~'
      # get all files from target dir
      p '------------------GETING IMAGES FROM TARGET DIRECTORY------------------'  
      files = Dir.glob(Rails.root + "public/item-images/*")
      p '------------------DONE------------------'
      p '~'

      p '------------------PARSING FILES------------------'
      begin
        files.each do |img|
          if img.present?
            # product id
            p_id = img.split('/').last.split('.').first.match(/^\d+$/).to_s
            product = Spree::Product.find_by_id(p_id)
            image = File.open(File.expand_path(img, __FILE__))

            # check if product exists
            if product.present?
              data = {viewable_id: product.master.id, viewable_type: 'Spree::Variant', attachment: image, alt: "position 1", position: 1} 
              attached_image = Spree::Image.create(data)
              product.images << attached_image
            end
          end
        end
      rescue => e
        p 'exited with error:'
        p e
        exit
      end
      p '------------------SUCCESSFUL------------------'
    end
  end
end
